# Istio Service Mesh Note

### What is Istio, and how does it work?
Istio is a service mesh that utilizes in its architecture Envoy Proxy as a sidecar proxy and istiod (d is for daemon) as a control plane that allows us to configure, discover, and manage our certificates. \
Istio is an open source service mesh that helps organizations run distributed, microservices-based apps anywhere.

### Why use Istio? 
Istio enables organizations to secure, connect, and monitor microservices, so they can modernize their enterprise apps more swiftly and securely.

### Core Capabilities of Istio
- Secure service-to-service communication through authentication and authorization.
- Implement policy layers supporting access controls, quotas and resource allocation.
- Automatic load balancing for HTTP, gRPC, WebSocket, and TCP traffic.
- Automatic metrics, logs, and traces for all traffic within a cluster, including cluster ingress and egress.
- Configure and control of inter-service communication through failovers, fault injection and routing rules.
- Implement policy layers supporting access controls, quotas and resource allocation.

### Service Mesh Benefits and Drawbacks
Service meshes can address some of the main challenges of managing communication between services, but they also have their 
drawbacks. 
Among the advantages of service meshes are:
- Service-to-service communication is simplified, both for containers and microservices.
- Communication errors are easier to diagnose, because they occur within their own infrastructure layer.
- You can develop, test and deploy applications faster.
- Sidecars placed alongside container clusters can effectively manage network services.
- Services meshes support various security features, including encryption, authentication and authorization.

Some of the drawbacks of service meshes are:
- The use of a service mesh can increase runtime instances.
- Communication involves an additional step—the service call first has to run through a sidecar proxy.
- Service meshes don’t support integration with other systems or services 
- They don’t address issues such as transformation mapping or routing type.

### Service Mesh Architecture and Concepts

There is a unique terminology for the functions and components of service meshes:
- Container orchestration framework: a service mesh architecture typically operates alongside a container orchestrator like Kubernetes or Nomad.
- Sidecar proxies: These run alongside each pod or instance and communicate with each other to route the traffic between the containers they run with. They are managed through the orchestration framework.
- Services and instances: Instances (or pods) are running copies of microservices. Clients usually access instances through services, which are scalable, fault-tolerant replicas of the instances.
- Service discovery: Instances need to discover available instances of the services they need to interact with. This usually involves a DNS lookup.
- Load balancing: Orchestration frameworks usually provide load balancing for the transport layer (Layer 4), but service meshes can also provide load balancing for the application layer (Layer 7).
- Encryption: Service meshes can encrypt requests and responses, and then decrypt them. They prioritize the reuse of established connections, which minimizes the need to create new ones and improves performance. Typically, traffic is encrypted using mutual TLS, with the public key infrastructure generating keys and certificates and keys used by sidecar proxies.
- Authentication and authorization: The service mesh can authorize and authenticate requests made from both outside and within the app, sending only validated requests to instances.
- Circuit-breaker pattern: This involves isolating unhealthy instances and bringing them back gradually if required.

The data plane is the part of a service mesh that manages network traffic between instances. The control plane generates and deploys the configurations that control the data plane. It usually includes (or can connect to) a command-line interface, an API and a graphical user interface.

[MoreDetails Click Here](https://www.aquasec.com/cloud-native-academy/container-security/service-mesh/)

### What Is Envoy Proxy?
Envoy Proxy is an open-source, high-performance edge and service proxy that is designed for cloud-native applications. It was originally developed at Lyft, and is now part of the Cloud Native Computing Foundation (CNCF) as a graduated project. \
Envoy Proxy is often used as a sidecar proxy, deployed alongside a service or container, and it provides a variety of features, including:

- Load balancing: Envoy can distribute incoming traffic across multiple instances of a service, improving performance and availability.
- Service discovery: Envoy can automatically discover and track the available instances of a service, making it easy to scale services up or down as needed.
- Security: Envoy supports a variety of authentication and authorization mechanisms, including mutual TLS (mTLS), OAuth, and JSON Web Tokens (JWTs).
- Observability: Envoy provides rich observability features, including metrics, logging, and tracing, making it easy to diagnose issues and monitor the health of a service.



**Is istio installed on this cluster?** \
Check if istio PODs are running using kubectl command in the terminal. \
`get pod -n istio-system`

**Install istio on Kubernetes cluster.** \
[Referance](https://istio.io/latest/docs/setup/getting-started/)
```
curl -L https://istio.io/downloadIstio | sh -
ls -la
cd istio-1.17.2/
export PATH=$PWD/bin:$PATH
istioctl install --set profile=demo -y
istioctl version
kubectl get pod -n istio-system
```

[Follow Official Doc](https://istio.io/latest/docs/setup/getting-started/)


**Can you identify which version of Istio was installed?** \
`istioctl version`

**Which of the following command is used to validate the istio configuration?** \
`istioctl analyze`


**What is Kiali?** \
Kiali is an open-source service mesh observability and management platform that is designed to work with Istio, one of the most popular service mesh technologies. Kiali is part of the Cloud Native Computing Foundation (CNCF) and is available under the Apache 2.0 license.

Kiali provides a range of features, including:
- Service mesh topology visualization: Kiali provides a graphical view of the Istio service mesh, showing how services are connected and how traffic is flowing between them.
- Service mesh health monitoring: Kiali provides real-time monitoring of the health and performance of the Istio service mesh, including metrics like request volume, latency, and error rates.
- Traffic flow visualization: Kiali can display the flow of traffic between services, including the protocols and ports being used.
- Tracing and logging: Kiali provides access to distributed tracing and log data for services running in the Istio service mesh.
- Security: Kiali provides visibility into Istio's security features, including mutual TLS (mTLS) authentication and authorization policies.

Overall, Kiali is a powerful tool that can help users understand and manage their Istio service mesh, making it easier to identify and troubleshoot issues.


### Istio Traffic Management 

Istio is a popular open-source service mesh technology that provides advanced traffic management capabilities. With Istio, users can easily control and manage the flow of traffic between services in a microservices-based application.

Here are some of the key traffic management features provided by Istio:


**What is Istio gateway** \
In Istio, a gateway is a component that enables traffic into the mesh from outside of the cluster. A gateway can be thought of as a load balancer or an API gateway that sits at the edge of the mesh and provides access to the services running inside. \
Gateway describes a load balancer operating at the edge of the mesh receiving incoming or outgoing HTTP/TCP connections. \
The Istio gateway is responsible for:
- Receiving incoming traffic from outside the cluster.
- Authenticating and authorizing the traffic.
- Encrypting and decrypting the traffic using Transport Layer Security (TLS).
- Routing the traffic to the appropriate service inside the mesh based on the rules defined in the VirtualService resource.

To use the Istio gateway, you first need to define a Gateway resource that describes the network configuration for the gateway. This includes the ports that the gateway listens on and the protocols that it supports.

Here's an example YAML definition for a basic Istio gateway: 
```
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: bookinfo-gateway
spec:
  selector:
    istio: ingressgateway
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
```
```
kubectl apply -f bookinfo-gateway.yaml
kubectl get gateway
kubectl describe gateway bookinfo-gateway
```

In this example, we have defined a gateway named "my-gateway" that listens on port 80 and supports the HTTP protocol. The "hosts" field is set to "*" to allow traffic from any host.
Once you have defined a gateway, you can create a VirtualService resource that defines the routing rules for incoming traffic. The VirtualService resource specifies how traffic should be routed based on the hostname, path, and other criteria. The Istio gateway uses these rules to determine which service to send the traffic to.

**What's Istio Subsets!!!**
In Istio, a subset is a group of instances of a service that share the same labels. Subsets can be used to define different versions of a service, or to route traffic to specific instances based on specific criteria.![[virtual_service-1.png]]

**What is Istio Virtual Service!!!** \
In Istio, a VirtualService is a resource that defines the rules for how incoming traffic should be routed within the service mesh. VirtualServices allow you to define fine-grained routing rules based on a variety of criteria, including HTTP headers, request paths, and source IP addresses.

![Istio Virtaul Service](/image/virtual_service-1.png "Istio Virtaul Service Architecture")

Definition for a simple bookinfo Istio VirtualService:
```
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
  - "bookinfo.app"
  gateways:
  - bookinfo-gateway
  http:
  - match:
    - uri:
        exact: /productpage
    - uri:
        prefix: /static
    - uri:
        exact: /login
    - uri:
        exact: /logout
    - uri:
        prefix: /api/v1/products
    route:
    - destination:
        host: productpage
        port:
          number: 9080
```

```
kubectl get virtualservice
kubectl describe virtualservice bookinfo
```

 Based on the weight Istio Virtual Service example: 
 ![Istio Virtaul Service](/image/weigt_based_vsvc.png "Based on the weight Istio Virtual Service")

In order to use Istio Virtual Services, you first need to configure the Kubernetes services that you want to expose using Istio. Once you have defined a Kubernetes service, you can then create a Virtual Service to route traffic to that service based on various conditions.
Here's an example of how you can configure a Kubernetes service and a corresponding Istio Virtual Service:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: reviews-v3
spec:
  replicas: 1
  selector:
    matchLabels:
      app: reviews
      version: v1
  template:
    metadata:
      labels:
        app: reviews
        version: v1
    spec:
      containers:
        - name: reviews
          image: myreviews:v1
          ports:
            - containerPort: 9080
---
apiVersion: v1
kind: Service
metadata:
  name: reviews
spec:
  ports:
    - name: http
      port: 9080
      targetPort: 9080
  selector:
    app: reviews
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews  #Service Name
  http:
    - route:
        - destination:
            host: reviews
            subset: v1
          weight: 99
        - destination:
            host: reviews
            subset: v2
          weight: 1

```

```
kubectl get virtualservice
kubectl describe virtualservice bookinfo
```

*Note: Defining subsets in the Service resource can be useful in scenarios where you have multiple deployments that share a common label, but each deployment has a different value for that label. You can define subsets based on that common label and use them in your VirtualServices to route traffic to the correct deployment.

**What is Destination Rules!!!** \
Istio Destination Rule is based on the labels assigned to the pods of a service in a Kubernetes deployment. When a Kubernetes deployment is created, it creates one or more pods with labels assigned to them. These labels can be used to select specific pods in a service, and can also be used to define subsets in a Destination Rule. \

In an Istio service mesh, a Virtual Service is used to define routing rules for incoming traffic. The Virtual Service can use the labels of the pods running the service to select a specific subset of the service to handle the traffic. \

When a Destination Rule is created, it defines one or more subsets of the service based on the labels assigned to the pods of the service. These subsets are used to define the different versions of the service that can be used to handle traffic. \
Here is an example of an Istio Destination Rule:
```
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews-destination
spec:
  host: reviews
  trafficPolicy:
    loadBalancer:
      simple: PASSTHROUGH
    subsets:
    - name: v1
      labels:
        version: v1
      trafficPolicy:
        loadBalancer:
          simple: ROUND_ROBIN
        weight: 99
    - name: v2
      labels:
        version: v2
      trafficPolicy:
        loadBalancer:
          simple: ROUND_ROBIN
        weight: 1
```

The host field in the spec section of a DestinationRule resource is used to specify the virtual service name (or service name, if no virtual service is defined) that the DestinationRule is targeting. So in this example, the host: reviews field is targeting the virtual service named reviews.

```
kubectl get deploy
kubectl get svc
kubectl get ep myapp
kubectl describe vs myapp
```

**Fault Injection!!!** \
Fault Injection is the process of deliberately introducing errors or faults into a system to test its ability to handle and recover from them. It is a testing technique used to evaluate the resilience and reliability of software systems, networks, or hardware devices.

In the context of Istio, Fault Injection can be used to test how well the application and the Istio service mesh handle failures and unexpected conditions. Istio provides a number of fault injection capabilities, including:

1.  Delays: Delay traffic for a specific amount of time, for example, to simulate a slow network or an overloaded service.
    
2.  Aborts: Terminate the request with a specific HTTP error code, for example, to simulate a service outage.
    
3.  Headers: Add, remove or modify HTTP headers, for example, to test how the application handles authentication headers.
    
4.  Response faults: Inject HTTP errors into the response body, for example, to test how the application handles malformed responses.
    

Fault Injection can be used to test and validate the resilience and fault-tolerance of your applications and service mesh, and to ensure that they can recover quickly from failures and unexpected conditions.

Here's an example of Fault Injection in Istio:
```
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
    - fault:
        delay:
          percent: 50
          fixedDelay: 5s
      route:
        - destination:
            host: reviews
            subset: v1
          weight: 99
        - destination:
            host: reviews
            subset: v2
          weight: 1
```

In this example, we are applying a fault injection to the `reviews` service, which will delay 50% of the traffic by 5 seconds. The delay is applied to all routes defined under the `http` section of the virtual service, which in this case are the `v1` and `v2` subsets of the `reviews` service.

**Istio Timeout** \
Timeouts are an important aspect of microservices communication as they help to ensure that requests do not hang indefinitely, which can lead to resource exhaustion and system instability. Istio allows you to configure timeouts at different levels, including the service, route, and destination levels.

Here is an example of how to configure a timeout for a service in Istio:
```
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: my-service
spec:
  hosts:
    - my-service.example.com
  http:
    - match:
        - uri:
            exact: /api/v1/my-endpoint
      route:
        - destination:
            host: my-service
            port:
              number: 8080
      timeout: 10s
```

In this example, we are configuring a timeout of 10 seconds for requests to the `/api/v1/my-endpoint` URI on the `my-service` host. Any requests that take longer than 10 seconds will be terminated by Istio. \

You can also configure timeouts at the route and destination levels. At the route level, you can set a timeout for a specific path or method within a service. At the destination level, you can set a timeout for requests to a specific version or subset of a service.

